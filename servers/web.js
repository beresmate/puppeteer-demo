const path = require('path');
const express = require('express');
const serveStatic = require('serve-static');
const cookieParser = require('cookie-parser');
const axios = require('axios');

const app = express();

app.use(cookieParser());
app.use(serveStatic(path.resolve(__dirname, '..', 'client')));
app.use(serveStatic(path.resolve(__dirname, '..', 'node_modules')));

app.post('/login', (req, res) => {
  res.cookie('puppet', 'authenticated').end();
});

app.post('/logout', (req, res) => {
  res.clearCookie('puppet').end();
});

app.post('/getpdf', async (req, res) => {
  try {
    const response = await axios.post('http://localhost:9090/snapshotplz', {
      token: req.cookies.puppet
    });

    res.sendFile(path.resolve(__dirname, '..', 'pdfs', response.data.filename), {
      headers: {
        'Content-Type': 'application/pdf'
      }
    });
  } catch (err) {
    res.status(401).end();
  }

});


app.listen(8080, () => {
  console.log('Webserver listening on http://localhost:8080');
});