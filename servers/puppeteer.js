const express = require('express');
const bodyParser = require('body-parser');
const puppeteer = require('puppeteer');
const path = require('path');

const app = express();

app.use(bodyParser());

app.post('/snapshotplz', async (req, res) => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  if (req.body.token) {
    await page.setCookie({
      name: 'puppet',
      value: req.body.token,
      url: 'http://localhost:8080'
    });
  }
  
  await page.goto('http://localhost:8080', {waitUntil: 'networkidle2'});
  await page.emulateMedia('screen');
  await page.pdf({
    path: path.resolve(__dirname, '..', 'pdfs', 'intranetz.pdf'),
    printBackground: true,
    width: 1024,
    height: 768
  });

  await browser.close();

  console.log('created PDF');

  res.json({
    filename: 'intranetz.pdf'
  });
});


app.listen(9090, () => {
  console.log('Puppeteer server listening on http://localhost:9090');
});