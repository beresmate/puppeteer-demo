const login = () => axios.post('/login');
const logout = () => axios.post('/logout');
const getPdf = () => axios.post('/getpdf', {}, {
  responseType: 'blob'
});

const toggleSecret = () => {
  const token = Cookies.get('puppet');
  const hidden = token === 'authenticated';
  const contentEl = document.getElementById('contentz');

  contentEl.classList.toggle('secret', !hidden);
};

const downloadBlob = responseBlob => {
  const blob = new Blob([responseBlob], { type: 'application/pdf' });
  const link = document.createElement('a');

  link.href = window.URL.createObjectURL(blob);
  link.download = `intranetz-${Date.now()}.pdf`;
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
};

document
  .getElementById('login')
  .addEventListener('click', async () => {
    await login();
    toggleSecret();
  });

document
  .getElementById('logout')
  .addEventListener('click', async () => {
    await logout();
    toggleSecret();
  });

document
  .getElementById('pdfz-please')
  .addEventListener('click', async () => {
    try {
      const {data} = await getPdf();

      downloadBlob(data);
    } catch (err) {
      console.error(err);
    }
  });

toggleSecret();